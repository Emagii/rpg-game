import pydot
import sqlite3

#Get dictionary results from sqlite
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

#Create db connection
connection = sqlite3.connect('rooms.db')
#Use our dict_factory to get dictionary results
connection.row_factory = dict_factory
cursor = connection.cursor()

#Get us some rooms
sql_command = "SELECT * FROM rooms;"
cursor.execute(sql_command)
rooms = cursor.fetchall()

# Create a directional graph object
graph = pydot.Dot(graph_type='digraph')

#Create a mapper for room_number that holds room name 
#and pydot node object for the room
roomMapper = {}
#Iterate all rooms
for room in rooms:
    #Instance a pydot node object named as our room
    node = pydot.Node(room['name'], style="filled")
    #Map our current room number to our room name and node instance
    roomMapper[room['room_number']] = {'room': room['name'], 'node': node}

    #Add the node instance for the current room to our graph
    graph.add_node(node)

#Now all rooms are added and the mapper is fully populated. 
#We can now iterate again to create connections between nodes, called "edges"
for room in rooms:
    #For every available direction
    for direction in ['n', 'e', 'w', 's']:
        #If our room as a connection in the current direction
        if room[direction] != None:
            #Add an edge (connection) from our currect room to the one in the connected direction
            graph.add_edge(pydot.Edge(
                #From node
                roomMapper[room['room_number']]['node'], 
                #To node
                roomMapper[room[direction]]['node']))

#Render PNG map
graph.write_png('dungeon_map.png')
