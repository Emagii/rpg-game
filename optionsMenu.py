def menu(x):
    print()
    print(f'You are now in {x}')
    print('What do you do?')
    print(' 1. Move on\n 2. Check for clues.\n 3. Quit')

    choicer = True
    while choicer == True:
        choice = input(' ')
        if not choice in ['1','2','3']:
            print('Not a valid option')
        else:
            choicer = False

    if choice == "1":
        return None

    elif choice == "2":
        print('Looking for clues...')
        

    elif choice == "3":
        quit()