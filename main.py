import sqlite3
import createRooms as cr
import optionsMenu as op

connection = sqlite3.connect("rooms.db")
cursor = connection.cursor()

#Used for debugging
cursor.execute("SELECT * FROM rooms") 
print("fetchall:")
result = cursor.fetchall() 
for r in result:
    print(r)


now_room = cr.first_room

## Testing menu
#op.menu(now_room)
##


def change_room(dirs):
    global now_room
    
    #Checking db for possible chosen direction
    cursor.execute(f"SELECT {dirs} FROM rooms WHERE name = '{now_room}'")
    res = cursor.fetchone()
    res_string = res[0]

    if res_string == None:
        print()
        print(f"No door at {dirs}")
        print()
        return None

    #If a possible direction is found in db
    cursor.execute(f"SELECT name FROM rooms WHERE room_number = {res_string}") 
    res = cursor.fetchone()
    res_string = res[0]
    now_room = res_string

    print(f'You moved to {now_room}')

    return now_room

#First menu
op.menu(now_room)

while True:
    while True: # Make sure direction choice is valid

        direction = input('Which direction? Use n,e,w,s to navigate ').lower()
        if not direction in ['n','e','w','s']:
            print("Not a valid direction")
        else:
            print()
            break
    
    change_room(direction)
    op.menu(now_room)
